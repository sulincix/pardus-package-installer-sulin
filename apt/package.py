import inary.db.installdb
class Package(object):
    """Representation of a package in a cache."""

    def __init__(self, pcache, pkgiter):
        # type: (apt.Cache, apt_pkg.Package) -> None
        idb=inary.db.installdb.InstallDB()
        """ Init the Package object """
        self._pkg = pkgiter
        self._pcache = pcache
        self._changelog = ""
        try:
            self.installed=idb.get_release(self._pkg)+"="+idb.get_version(self._pkg)[0]
        except:
            self.installed="0=0"
        print(pkgiter)
        self._sections=[{"Version":"1.0"}]
        
    def is_installed(self):
        # type: () -> Optional[Version]
        try:
            idb.get_release(self._pkg)
            return True
        except:
            return False
