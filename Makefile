build:
	: please run make install
install:
	mkdir -p "$(DESTDIR)/usr/lib/pardus-package-installer/" || true
	cp -prvf  apt $(DESTDIR)/usr/lib/pardus-package-installer/
	cp -prvf ui $(DESTDIR)/usr/lib/pardus-package-installer/
	cp -prvf parduspackageinstaller $(DESTDIR)/usr/lib/pardus-package-installer/
	mkdir -p $(DESTDIR)/usr/bin
	install pardus-package-installer $(DESTDIR)/usr/bin
	install pardus-package-installer-action $(DESTDIR)/usr/bin
	cp icon.svg $(DESTDIR)/usr/lib/pardus-package-installer/
	mkdir -p $(DESTDIR)/usr/share/applications
	install tr.org.pardus.package-installer.desktop $(DESTDIR)/usr/share/applications
